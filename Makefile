CFLAGS = -Wall -Werror -O2 -Wl,-s

all: mutantwatch

cross: mutantwatch.exe

mutantwatch: mutantwatch.c
	$(CC) $(CFLAGS) -o mutantwatch mutantwatch.c

mutantwatch.exe: mutantwatch.c
	i686-w64-mingw32-gcc $(CFLAGS) -o mutantwatch.exe mutantwatch.c
	
clean:
	$(RM) mutantwatch mutantwatch.exe
	

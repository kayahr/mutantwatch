/*
 * mutantwatch - Checks savegame for mutations and create a backup if no
 * mutation has been found. When mutation has been found then program exits
 * with a warning message.
 * 
 * Copyright (C) 2012 Klaus Reimer <k@ailis.de>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *  
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT
 * OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <sys/stat.h> 
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#ifdef WIN32
#include <windows.h>
#endif

static char *files[] = { "DMAP", "FMAP", "KMAP", "DISK1", NULL };

static long lastcheck = 0;

static void copyfile(char *sourcename, char *destname)
{
    FILE *source, *dest;
    char buffer[8192];
    int read;
    
    source = fopen(sourcename, "rb");
    if (!source)
    {
        fprintf(stderr, "Unable to open %s for reading: %s\n", sourcename,
            strerror(errno));
        exit(1);
    }
    dest = fopen(destname, "wb");
    if (!dest)
    {
        fprintf(stderr, "Unable to open %s for writing: %s\n", destname,
            strerror(errno));
        exit(1);
    }
    
    while ((read = fread(buffer, 1, 8192, source)))
    {
        if (fwrite(buffer, 1, read, dest) != read)
        {
            fprintf(stderr, "Unable to write to %s: %s\n", destname,
                strerror(errno));
            exit(1);
        }
    }
    if (ferror(source))
    {
        fprintf(stderr, "Unable to read from %s: %s\n", sourcename,
            strerror(errno));
        exit(1);
    }
    fclose(source);
    fclose(dest);
}

static void backup()
{
    int i;
    char *source;
    char dest[16];

    #ifdef WIN32
    mkdir("bak");
    #else
    mkdir("bak", 0755);    
    #endif
    
    for (i = 0; files[i]; ++i)
    {
        source = files[i];
        strcpy(dest, "bak/");
        strcat(dest, source);
        copyfile(source, dest);
    }
}

static void ioerror(char *message)
{
    fprintf(stderr, "%s: %s\n", message, strerror(errno));
    exit(1);
}

static int checkgame()
{
    FILE *file;
    unsigned char c, max_c, a, b;
    char name[13];
    struct stat fst;
    time_t timer;
    char time_buffer[9];
    struct tm *tm_info;
    
    if (stat("DISK1", &fst) != 0)
        ioerror("Unable to check timestamp of DISK1 file");
    
    if (fst.st_mtime == lastcheck) return 1;
    lastcheck = fst.st_mtime;
    
    time(&timer);
    tm_info = localtime(&timer);
    strftime(time_buffer, 9, "%H:%M:%S", tm_info);
    
    printf("%s Checking for mutations... ", time_buffer);
    
    file = fopen("DISK1", "rb");
    if (!file) ioerror("Unable to open DISK1 file");
    
    fseek(file, 0x31, SEEK_SET);
    if (!fread(&max_c, 1, 1, file))
        ioerror("Unable to read number of characters from DISK1 file");

    name[12] = 0;
    for (c = 0; c < max_c; ++c)
    {
        fseek(file, 0x3a + 332 * c, SEEK_SET);
        if (!fread(name, 1, 12, file))
            ioerror("Unable to read character name from DISK1 file");
        fseek(file, 0x8d + 332 * c, SEEK_SET);
        if (!fread(&a, 1, 1, file))
            ioerror("Unable to read inactive mutations from DISK1 file");
        fseek(file, 0x8f + 332 * c, SEEK_SET);
        if (!fread(&b, 1, 1, file))
            ioerror("Unable to read postponed mutation effects from DISK1 file");
        
        if (a || b)
        {
            printf("\n\n!!! %s has started mutating !!!\n\n", name);
            fclose(file);
            return 0;
        }
    }

    fclose(file);
    printf("Clean. Creating backup... ");
    backup();    
    printf("Done.\n");
    return 1;
}

int main(int argc, char *argv[])
{
    while (checkgame())
    {
        #ifdef WIN32
        Sleep(250);
        #else
        usleep(250);
        #endif
    }
    return 0;
}

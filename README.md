mutantwatch
===========

Command-line program which monitors the `DISK1` file of the game 
[Fountain of Dreams](http://en.wikipedia.org/wiki/Fountain_of_Dreams) 
for inactive mutations.  If no inactive mutations have been found then a
backup of the `DISK1` file and the `MAP` files of the game are created in 
the `bak` sub directory.

Purpose of this program is preventing and researching the annoying mutation
process of the game.

Compile
-------

On Linux (Or any other Unix-like system) do this:

    $ git clone git://github.com/kayahr/mutantwatch.git
    $ cd mutantwatch
    $ make

Windows users may prefer downloading the 
[pre-compiled EXE file](https://github.com/downloads/kayahr/mutantwatch/mutantwatch.exe)

Usage
-----

Copy the `mutantwatch` program into the game directory (Which contains the
`DISK1` file) and run it.  Then play the game in Dosbox as usual.  Every
time you save the game check the output of `mutantwatch`.  If it exits with
a `XYZ has started mutating!` message then exit the game and copy the backup
files from the `bak` directory back into the game directory and start the
game again.
